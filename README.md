### gitdown
> Simple tool to sync (pull down) git repositories from a plain text list.

##### How to use

> You will need to install the [Odin compiler](http://odin-lang.org), [make](https://www.gnu.org/software/make) utility
and [UPX](https://upx.github.io) executable compressor to build from repository source.

> It is recommend to build the [Odin compiler from source](http://odin-lang.org/docs/install), retrieved with Git.
> I recommend building on [Ubuntu](https://ubuntu.com/download) (or a Docker container or server VM of Ubuntu).

On Ubuntu - you can install the following packages, before building the compiler, with `APT`):
<pre># apt install upx-ucl clang llvm build-essential</pre>

##### Build from source and install
<pre>$ make</pre>
<pre># make install</pre>

##### Usage

Please see `gitdown --help`, `gitdown.sh` and `repos.txt` for example usage.

