TARGET=gitdown
COMPILER=odin

make:
	$(COMPILER) build .
	strip $(TARGET)

install:
	@echo Please run this as sudo/doas.
	upx -9 -o /usr/bin/$(TARGET) $(TARGET)

uninstall:
	@echo Please run this as sudo/doas.
	rm /usr/bin/$(TARGET)

clean:
	rm $(TARGET)
