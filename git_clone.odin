/**
    * gitdown
    * Simple tool to sync git repositories from a plain text list.
    * Copyright 2023 Sam St-Pettersen.
    * Released under the MIT License.
*/

package main

import "core:fmt"
import "core:os"
import "core:c/libc"
import "core:strings"
import "core:time"

formatDigit :: proc(digit: int) -> string {
    d := fmt.aprintf("%d", digit)
    return strings.right_justify(d, 2, "0")
}

gitCloneAll :: proc(verbose: bool, reposList: []string, root: string, target: string, utcOffset: int) {
    for repo in reposList {
        gitClone(verbose, strings.cut(repo, 0, (len(repo) - 4)), root, target, utcOffset)
    }
}

gitClone :: proc(verbose: bool, repo: string, root: string, target: string, utcOffset: int) {
    cloning: bool = false
    dir: string = fmt.aprintf("%s%s", target, repo)

    if os.exists(dir) {
        now := time.now()
        y, m, d := time.date(now)
        h, mn, s := time.clock_from_time(now)
        day: string = formatDigit(d)
        month := fmt.aprintf("%d", m)
        month = strings.right_justify(month, 2, "0")
        hour: string = formatDigit(h + utcOffset)
        minute: string = formatDigit(mn)
        second: string = formatDigit(s)

        if verbose {
            fmt.printf("[%d-%s-%s %s:%s:%s]\n", y, month, day, hour, minute, second)
            fmt.printf("Pulling down over existing repo %s...\n", repo)
        }

        cmd: string = fmt.aprintf("git -C %s pull", dir)
        git, _ := strings.clone_to_cstring(cmd)
        libc.system(git)
    }
    else {
        now := time.now()
        y, m, d := time.date(now)
        h, mn, s := time.clock_from_time(now)
        day: string = formatDigit(d)
        month := fmt.aprintf("%d", m)
        month = strings.right_justify(month, 2, "0")
        hour: string = formatDigit(h + utcOffset)
        minute: string = formatDigit(mn)
        second: string = formatDigit(s)

        cloning = true
        if verbose {
            fmt.printf("[%d-%s-%s %s:%s:%s]\n", y, month, day, hour, minute, second)
            fmt.printf("Cloning new repo %s...\n", repo)
        }

        cmd: string = fmt.aprintf("git clone %s%s %s", root, repo, dir)
        git, _ := strings.clone_to_cstring(cmd)
        libc.system(git)
    }

    if cloning {
        cmd: string = fmt.aprintf("git -C %s config pull.ff only", dir)
        git, _ := strings.clone_to_cstring(cmd)
        libc.system(git)
    }
}
