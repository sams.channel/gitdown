/**
    * gitdown
    * Simple tool to sync git repositories from a plain text list.
    * Copyright 2023 Sam St-Pettersen.
    * Released under the MIT License.
*/

package main

import "core:fmt"
import "core:os"
import "core:strconv"
import "core:strings"

displayVersion :: proc() -> int {
    fmt.println("gitdown v0.1.0 (Odin)")
    return 0
}

displayError :: proc(program: string, err: string) -> int {
    fmt.printf("Error: %s.\n\n", err)
    return displayUsage(program, -1)
}

displayUsage :: proc(program: string, exitCode: int) -> int {
    fmt.println("gitdown")
    fmt.println("Simple tool to sync git repositories from a plain text list.")
    fmt.println("Copyright 2023 Sam St-Pettersen <s.stpettersen@proton.me>")
    fmt.println("\nReleased under the MIT License.\n")
    fmt.printf("Usage: %s [-h | -v][(-q) -l <repos.txt> -r <repo_url_root> -t <target_root_dir>]\n", program)
    fmt.println("\nOptions are:\n")
    fmt.println("-l | --list: Set the list of repos to clone/pull down.")
    fmt.println("-r | --root: Set the repository URL root (including trailing '/').")
    fmt.println("-t | --target: Set the traget directory for cloned repos.")
    fmt.println("-q | --quiet: Don't write to stdout.")
    fmt.println("-h | --help: Display this usage information and exit.")
    fmt.println("-v | --version: Display version information and exit.\n")
    return exitCode
}

getUtcOffset :: proc() -> int {
    data, ok := os.read_entire_file(".gitdown_utc_offset", context.allocator)
    if !ok {
        return 0
    }
    defer delete(data, context.allocator)

    it := string(data)
    for utcOffset in strings.split_lines_iterator(&it) {
        return strconv.atoi(utcOffset)
    }

    return 0
}

cloneRepos :: proc(program: string, verbose: bool, list: string, root: string, target: string) -> int {
    reposList := make([dynamic]string, 0, 0)
    data, ok := os.read_entire_file(list, context.allocator)
    if !ok {
        return displayError(program,
        fmt.aprintf("Could not load list file:\n\"%s\"", list))
    }
    defer delete(data, context.allocator)

    it := string(data)
    for repo in strings.split_lines_iterator(&it) {
        append(&reposList, repo)
    }

    gitCloneAll(verbose, reposList[:], root, target, getUtcOffset())

    return 0
}

main :: proc() {
    exitCode: int = 0
    program : string : "gitdown"

    verbose: bool = true
    list: string = ""
    root: string = ""
    target: string = ""

    if len(os.args) > 1 {
        i : int = 0
        for a in os.args {
            switch(a) {
                case "--quiet":
                    fallthrough
                case "-q":
                    verbose = false
                case "--list":
                    fallthrough
                case "-l":
                    list = os.args[i+1]
                case "--root":
                    fallthrough
                case "-r":
                    root = os.args[i+1]
                case "-t":
                    fallthrough
                case "--target":
                    target = os.args[i+1]
                case "--help":
                    fallthrough
                case "-h":
                    exitCode = displayUsage(program, 0)
                    os.exit(exitCode)
                case "--version":
                    fallthrough
                case "-v":
                    exitCode = displayVersion()
                    os.exit(exitCode)
            }
            i += 1
        }

        if len(list) == 0 || len(root) == 0 || len(target) == 0 {
            exitCode = displayError(program, "Please specify required options")
            os.exit(exitCode)
        }

        exitCode = cloneRepos(program, verbose, list, root, target)
    }
    else {
        exitCode = displayError(program, "No options specified");
    }

    os.exit(exitCode)
}
