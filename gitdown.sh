#!/bin/sh
# Keep repos in sync (backed up) with gitdown tool.
eval `keychain --agents ssh --eval id_rsa`
curl -s http://pi4srv.www/repos.txt > repos.txt
echo Cloning on internal SSD:
gitdown -l repos.txt -r git@pi4srv:/srv/git/ -t ~/Projects/
